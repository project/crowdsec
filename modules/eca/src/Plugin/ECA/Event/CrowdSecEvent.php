<?php

namespace Drupal\eca_crowdsec\Plugin\ECA\Event;

use Drupal\crowdsec\Event\CrowdSecEvents;
use Drupal\crowdsec\Event\IpBanned;
use Drupal\crowdsec\Event\IpBaseEvent;
use Drupal\crowdsec\Event\IpBlocked;
use Drupal\crowdsec\Event\IpSignalled;
use Drupal\crowdsec\Event\IpUnBanned;
use Drupal\crowdsec\Event\ScenarioList;
use Drupal\crowdsec\Event\SignalScenarioList;
use Drupal\eca\Attributes\Token;
use Drupal\eca\Plugin\ECA\Event\EventBase;

/**
 * Plugin implementation of the CrowdSec ECA Events.
 *
 * @EcaEvent(
 *   id = "crowdsec",
 *   deriver = "Drupal\eca_crowdsec\Plugin\ECA\Event\CrowdSecEventDeriver",
 *   eca_version_introduced = "1.0.0"
 * )
 */
class CrowdSecEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    return [
      'scenariolist' => [
        'label' => 'Scenario list',
        'event_name' => CrowdSecEvents::SCENARIO_LIST_BUILD,
        'event_class' => ScenarioList::class,
      ],
      'signalscenariolist' => [
        'label' => 'Signal scenario list',
        'event_name' => CrowdSecEvents::SIGNAL_SCENARIO_LIST_BUILD,
        'event_class' => SignalScenarioList::class,
      ],
      'blocked' => [
        'label' => 'IP blocked',
        'event_name' => CrowdSecEvents::IP_BLOCKED,
        'event_class' => IpBlocked::class,
      ],
      'banned' => [
        'label' => 'IP banned',
        'event_name' => CrowdSecEvents::IP_BANNED,
        'event_class' => IpBanned::class,
      ],
      'unbanned' => [
        'label' => 'IP unbanned',
        'event_name' => CrowdSecEvents::IP_UNBANNED,
        'event_class' => IpUnBanned::class,
      ],
      'signalled' => [
        'label' => 'IP signalled',
        'event_name' => CrowdSecEvents::IP_SIGNALLED,
        'event_class' => IpSignalled::class,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  #[Token(
    name: 'event',
    description: 'The event.',
    classes: [
      ScenarioList::class,
      IpBaseEvent::class,
    ],
    properties: [
      new Token(
        name: 'crowdsec_scenario_list',
        description: 'The CrowdSec object.',
        classes: [
          ScenarioList::class,
        ],
      ),
      new Token(
        name: 'crowdsec_ip',
        description: 'The ip address.',
        classes: [
          IpBaseEvent::class,
        ],
      ),
      new Token(
        name: 'crowdsec_scenario',
        description: 'The CrowdSec scenario.',
        classes: [
          IpSignalled::class,
        ],
      ),
    ],
  )]
  protected function buildEventData(): array {
    $event = $this->event;
    $data = [];

    if ($event instanceof ScenarioList) {
      $data += [
        'crowdsec_scenario_list' => $event->getScenarios(),
      ];
    }
    elseif ($event instanceof IpBaseEvent) {
      $data += [
        'crowdsec_ip' => $event->getIp(),
      ];
      if ($event instanceof IpSignalled) {
        $data += [
          'crowdsec_scenario' => $event->getScenario(),
        ];
      }
    }

    $data += parent::buildEventData();
    return $data;
  }

}
