<?php

namespace Drupal\eca_crowdsec\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventDeriverBase;

/**
 * Deriver for CrowdSec ECA event plugins.
 */
class CrowdSecEventDeriver extends EventDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected function definitions(): array {
    return CrowdSecEvent::definitions();
  }

}
