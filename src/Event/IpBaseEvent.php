<?php

namespace Drupal\crowdsec\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Abstract class for IP address events.
 */
abstract class IpBaseEvent extends Event {

  /**
   * The IP address.
   *
   * @var string
   */
  protected string $ip;

  /**
   * Constructs the IP address event.
   */
  public function __construct(string $ip) {
    $this->ip = $ip;
  }

  /**
   * Returns the IO address.
   *
   * @return string
   *   The IP address.
   */
  public function getIp(): string {
    return $this->ip;
  }

}
