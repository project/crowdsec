<?php

namespace Drupal\crowdsec\Event;

/**
 * Event for a signalled IP address.
 */
class IpSignalled extends IpBaseEvent {

  /**
   * The scenario.
   *
   * @var string
   */
  protected string $scenario;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $ip, string $scenario) {
    parent::__construct($ip);
    $this->scenario = $scenario;
  }

  /**
   * Returns the scenario.
   *
   * @return string
   *   The scenario.
   */
  public function getScenario(): string {
    return $this->scenario;
  }

}
