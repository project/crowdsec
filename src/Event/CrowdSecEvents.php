<?php

namespace Drupal\crowdsec\Event;

/**
 * Events dispatched by the crowdsec module.
 */
final class CrowdSecEvents {

  /**
   * Dispatches when the list of scenarios gets build.
   *
   * @Event
   *
   * @var string
   */
  public const SCENARIO_LIST_BUILD = 'crowdsec.scenario.list_build';

  /**
   * Dispatches when the list of signalling scenarios gets build.
   *
   * @Event
   *
   * @var string
   */
  public const SIGNAL_SCENARIO_LIST_BUILD = 'crowdsec.signal.scenario.list_build';

  /**
   * Dispatches when a request from an ip address was blocked.
   *
   * @Event
   *
   * @var string
   */
  public const IP_BLOCKED = 'crowdsec.ip.blocked';

  /**
   * Dispatches when an ip address was banned.
   *
   * @Event
   *
   * @var string
   */
  public const IP_BANNED = 'crowdsec.ip.banned';

  /**
   * Dispatches when an ip address was un-banned.
   *
   * @Event
   *
   * @var string
   */
  public const IP_UNBANNED = 'crowdsec.ip.unbanned';

  /**
   * Dispatches when an ip address was signalled upstream.
   *
   * @Event
   *
   * @var string
   */
  public const IP_SIGNALLED = 'crowdsec.ip.signalled';

}
