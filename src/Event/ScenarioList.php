<?php

namespace Drupal\crowdsec\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event for building the scenario list.
 */
class ScenarioList extends Event {

  /**
   * List of scenarios.
   *
   * @var array
   */
  protected array $scenarios;

  /**
   * Constructs the scenario list event.
   */
  public function __construct(array &$scenarios) {
    $this->scenarios = &$scenarios;
  }

  /**
   * Get the list of scenarios.
   *
   * @return array
   *   The list of scenarios.
   */
  public function &getScenarios(): array {
    return $this->scenarios;
  }

}
