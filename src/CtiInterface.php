<?php

declare(strict_types=1);

namespace Drupal\crowdsec;

/**
 * Interface for the Crowdsec CTI API services.
 */
interface CtiInterface {

  /**
   * Provides smoke information about a given IP address.
   *
   * @param string $ip
   *   The IP address.
   *
   * @return array
   *   The smoke data.
   *
   * @see https://crowdsecurity.github.io/cti-api/#/Freemium/get_smoke__ip_
   */
  public function getSmoke(string $ip): array;

}
