<?php

namespace Drupal\crowdsec\Drush\Commands;

use CrowdSec\CapiClient\ClientException;
use Drupal\crowdsec\Buffer;
use Drupal\crowdsec\Client;
use Drupal\crowdsec\Form\Settings;
use Drush\Attributes\Command;
use Drush\Attributes\Argument;
use Drush\Attributes\Usage;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file.
 */
class CrowdSecCommands extends DrushCommands {

  /**
   * The CrowdSec client service.
   *
   * @var \Drupal\crowdsec\Client
   */
  protected Client $client;

  /**
   * The CrowdSec buffer service.
   *
   * @var \Drupal\crowdsec\Buffer
   */
  protected Buffer $buffer;

  /**
   * Constructs the CrowdSec event subscriber service.
   *
   * @param \Drupal\crowdsec\Client $client
   *   The CrowdSec client service.
   * @param \Drupal\crowdsec\Buffer $buffer
   *   The CrowdSec buffer service.
   */
  public function __construct(Client $client, Buffer $buffer) {
    $this->client = $client;
    $this->buffer = $buffer;
    parent::__construct();
  }

  /**
   * Return an instance of these Drush commands.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \Drupal\crowdsec\Drush\Commands\CrowdSecCommands
   *   The instance of Drush commands.
   */
  public static function create(ContainerInterface $container): CrowdSecCommands {
    return new CrowdSecCommands(
      $container->get('crowdsec.client'),
      $container->get('crowdsec.buffer'),
    );
  }

  /**
   * Enroll CrowdSec account.
   *
   * @param string $name
   *   Name of this instance.
   * @param string $enrollkey
   *   Enroll key you obtained from the CrowdSec console.
   */
  #[Command(name: 'crowdsec:enroll', aliases: [])]
  #[Argument(name: 'name', description: 'Name of this instance.')]
  #[Argument(name: 'enrollkey', description: 'Enroll key you obtained from the CrowdSec console.')]
  #[Usage(name: 'crowdsec:enroll NAME ENROLLKEY', description: 'Enroll CrowdSec account.')]
  public function enroll(string $name, string $enrollkey): void {
    $this->client->watcher()->enroll($name, TRUE, $enrollkey);
  }

  /**
   * Send buffered signals to CrowdSec.
   */
  #[Command(name: 'crowdsec:signal', aliases: [])]
  #[Usage(name: 'crowdsec:signal', description: 'Send buffered signals CrowdSec.')]
  public function sendSignals(): void {
    $this->buffer->push();
  }

  /**
   * Collect data from CrowdSec.
   */
  #[Command(name: 'crowdsec:collect', aliases: [])]
  #[Usage(name: 'crowdsec:collect', description: 'Collect data from CrowdSec.')]
  public function collect(): void {
    $this->client->refresh();
  }

  /**
   * Test CrowdSec by sending a signal to ban an IP address.
   *
   * @param string $ip
   *   The IP address to signal.
   */
  #[Command(name: 'crowdsec:test:signal', aliases: [])]
  #[Argument(name: 'ip', description: 'The IP address to signal.')]
  #[Usage(name: 'crowdsec:test:signal 1.2.3.4', description: 'Test CrowdSec by sending a signal to ban the IP 1.2.3.4.')]
  public function testSignalIp(string $ip): void {
    $watcher = $this->client->watcher();
    try {
      $signal = $watcher->buildSimpleSignalForIp($ip, Settings::SCENARIO_FLOOD, NULL);
      $watcher->pushSignals([$signal]);
    }
    catch (ClientException $e) {
      $this->logger->critical($e->getMessage());
    }
  }

  /**
   * Test CrowdSec by verifying an IP address.
   *
   * @param string $ip
   *   The IP address to verify.
   */
  #[Command(name: 'crowdsec:test:ip', aliases: [])]
  #[Argument(name: 'ip', description: 'The IP address to verify.')]
  #[Usage(name: 'crowdsec:test:ip 1.2.3.4', description: 'Test CrowdSec by verifying the IP 1.2.3.4.')]
  public function testIp(string $ip): void {
    print($this->client->verifyIp($ip));
  }

}
