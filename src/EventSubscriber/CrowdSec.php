<?php

namespace Drupal\crowdsec\EventSubscriber;

use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\crowdsec\Buffer;
use Drupal\crowdsec\Client;
use Drupal\crowdsec\Form\Settings;
use Drupal\user\Event\UserEvents;
use Drupal\user\Event\UserFloodEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * CrowdSec event subscriber.
 */
class CrowdSec extends HttpExceptionSubscriberBase {

  /**
   * The CrowdSec client service.
   *
   * @var \Drupal\crowdsec\Client
   */
  protected Client $client;

  /**
   * The CrowdSec buffer service.
   *
   * @var \Drupal\crowdsec\Buffer
   */
  protected Buffer $buffer;

  /**
   * Constructs the CrowdSec event subscriber service.
   *
   * @param \Drupal\crowdsec\Client $client
   *   The CrowdSec client service.
   * @param \Drupal\crowdsec\Buffer $buffer
   *   The CrowdSec buffer service.
   */
  public function __construct(Client $client, Buffer $buffer) {
    $this->client = $client;
    $this->buffer = $buffer;
  }

  /**
   * {@inheritdoc}
   */
  protected static function getPriority(): int {
    // Get work done even before Fast404.
    return 205;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats(): array {
    return ['html'];
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[UserEvents::FLOOD_BLOCKED_IP] = ['onBlockIp'];
    $events[UserEvents::FLOOD_BLOCKED_USER] = ['onBlockIp'];
    return $events;
  }

  /**
   * Handles all 4xx errors for HTML.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The event to process.
   */
  public function on4xx(ExceptionEvent $event): void {
    $ip = $event->getRequest()->getClientIp();
    if (empty($ip)) {
      return;
    }
    $this->buffer->addWhisperSignal($ip);
  }

  /**
   * Will be called when flood control bans an IP.
   *
   * @param \Drupal\user\Event\UserFloodEvent $event
   *   The user flood event.
   * @param string $eventName
   *   The name of the event.
   */
  public function onBlockIp(UserFloodEvent $event, string $eventName): void {
    $this->buffer->addSignal(Settings::SCENARIO_FLOOD, $event->getIp(), $event->getWindow());
  }

}
