<?php

namespace Drupal\crowdsec\Plugin\QueueWorker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Url;
use Drupal\ban\BanIpManagerInterface;
use Drupal\crowdsec\Client;
use Drupal\crowdsec\Event\CrowdSecEvents;
use Drupal\crowdsec\Event\IpUnBanned;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * CrowdSec queue worker.
 *
 * @QueueWorker(
 *   id = "crowdsec",
 *   title = @Translation("CrowdSec"),
 *   cron = {"time" = 3}
 * )
 */
final class CrowdSec extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected QueueInterface $queue;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The ban IP manager.
   *
   * @var \Drupal\ban\BanIpManagerInterface
   */
  protected BanIpManagerInterface $banIpManager;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Constructs the CrowdSec queue worker.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\ban\BanIpManagerInterface $banIpManager
   *   The ban IP manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, LoggerChannelInterface $logger, QueueFactory $queueFactory, TimeInterface $time, BanIpManagerInterface $banIpManager, EventDispatcherInterface $eventDispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->queue = $queueFactory->get('crowdsec', TRUE);
    $this->time = $time;
    $this->banIpManager = $banIpManager;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): CrowdSec {
    return new CrowdSec(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.crowdsec'),
      $container->get('queue'),
      $container->get('datetime.time'),
      $container->get('ban.ip_manager'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    if ($data['type'] === 'unban' && $data['due'] <= $this->time->getRequestTime()) {
      $this->banIpManager->unbanIp($data['ip']);
      $this->logger->info('Un-banned ip @ip', [
        '@ip' => $data['ip'],
        'link' => Link::fromTextAndUrl('Link', Url::fromUri(Client::CROWDSEC_URL_CTI . $data['ip']))->toString(),
      ]);
      $this->eventDispatcher->dispatch(new IpUnBanned($data['ip']), CrowdSecEvents::IP_UNBANNED);
      return;
    }

    // Item not processed, let's re-queue.
    $this->queue->createItem($data);
  }

}
