<?php

namespace Drupal\crowdsec\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\crowdsec\Event\CrowdSecEvents;
use Drupal\crowdsec\Event\ScenarioList;
use Drupal\crowdsec\Event\SignalScenarioList;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Configure crowdsec settings for this site.
 */
class Settings extends ConfigFormBase {

  public const SCENARIO_BAN = 'drupal/core-ban';
  public const SCENARIO_FLOOD = 'drupal/auth-bruteforce';
  public const SCENARIO_WHISPER = 'drupal/4xx-scan';

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Settings {
    $instance = parent::create($container);
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * Provides a list of available scenarios that can be signaled upstream.
   *
   * @return array
   *   The available scenarios for upstream signalling.
   */
  protected function signalScenarios(): array {
    $scenarios = [
      $this::SCENARIO_BAN => $this->t('Drupal bans from administrators'),
      $this::SCENARIO_FLOOD => $this->t('Drupal bans from flood control'),
      $this::SCENARIO_WHISPER => $this->t('Drupal bans from whispers'),
    ];
    // Allow other modules to add more scenarios.
    $this->eventDispatcher->dispatch(new SignalScenarioList($scenarios), CrowdSecEvents::SIGNAL_SCENARIO_LIST_BUILD);
    return $scenarios;
  }

  /**
   * Provides a list of available scenarios.
   *
   * @return array
   *   The available scenarios.
   */
  protected function scenarios(): array {
    $scenarios = $this->signalScenarios() + [
      'crowdsecurity/http-backdoors-attempts' => $this->t('Detect attempt to common backdoors'),
      'crowdsecurity/http-bad-user-agent' => $this->t('Detect bad user-agents'),
      'crowdsecurity/http-crawl-non_statics' => $this->t('Detect aggressive crawl from single ip'),
      'crowdsecurity/http-probing' => $this->t('Detect site scanning/probing from a single ip'),
      'crowdsecurity/http-path-traversal-probing' => $this->t('Detect path traversal attempt'),
      'crowdsecurity/http-sensitive-files' => $this->t('Detect attempt to access to sensitive files (.log, .db ..) or folders (.git)'),
      'crowdsecurity/http-sqli-probing' => $this->t('A scenario that detects SQL injection probing with minimal false positives'),
      'crowdsecurity/http-xss-probing' => $this->t('A scenario that detects XSS probing with minimal false positives'),
      'crowdsecurity/http-w00tw00t' => $this->t('Detect w00tw00t'),
      'crowdsecurity/http-generic-bf' => $this->t('Detect generic http brute force'),
      'crowdsecurity/http-open-proxy' => $this->t('Detect scan for open proxy'),
    ];
    // Allow other modules to add more scenarios.
    $this->eventDispatcher->dispatch(new ScenarioList($scenarios), CrowdSecEvents::SCENARIO_LIST_BUILD);
    return $scenarios;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'crowdsec_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['crowdsec.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('crowdsec.settings');

    $form['log_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Log level'),
      '#options' => RfcLogLevel::getLevels(),
      '#default_value' => $config->get('log_level'),
      '#weight' => -20,
      '#description' => $this->t('Select the log level to be used for logging module messages. Higher levels capture more detailed logs, while lower levels capture only essential messages.'),
    ];

    $form['env'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment'),
      '#options' => [
        'dev' => $this->t('Development'),
        'prod' => $this->t('Production'),
      ],
      '#default_value' => $config->get('env'),
      '#required' => TRUE,
      '#description' => $this->t('Select the environment type. "Development" enables more detailed debugging and verbose logging for testing purposes, while "Production" optimizes performance and restricts logging to only critical issues. Switching the environment will reset the site’s ID and result in the loss of historical data.'),
    ];
    $form['warning'] = [
      '#type' => 'item',
      '#markup' => '<p><strong>' . $this->t('Attention: if you change the environment, CrowdSec will have to assign a new ID to this Drupal site and all legacy data will be lost.') . '</strong></p>',
      '#states' => [
        'invisible' => [
          'select[name="env"]' => ['value' => $config->get('env')],
        ],
      ],
    ];

    $form['api_timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('API Timeout'),
      '#default_value' => $config->get('api_timeout'),
      '#required' => TRUE,
      '#min' => -1,
      '#max' => 999,
      '#description' => $this->t('Specify the timeout in seconds for the API request to CrowdSec. A negative value will disable the timeout. If the timeout occurs, the request to CrowdSec fails, and the module logs a critical error. Cached data is used as a fallback to continue processing, which may affect the accuracy of remediation decisions. If no cache is available, the default behavior will bypass remediation.'),
    ];

    $form['cti_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CTI API Key'),
      '#default_value' => $config->get('cti_api_key'),
      '#description' => $this->t('Provide your CTI API Key. This is optional and only required if you want to receive smoke data about IP addresses. You can <a href=":api_url" target="_blank">obtain your key</a> when you <a href=":account_url" target="_blank">create a free account</a>.', [
        ':api_url' => 'https://app.crowdsec.net/settings/cti-api-keys',
        ':account_url' => 'https://app.crowdsec.net',
      ]),
    ];

    $form['whisper'] = [
      '#type' => 'details',
      '#title' => $this->t('Whisper'),
      '#description' => $this->t('Automatically bans IPs that often send requests resulting in 4xx status codes.'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['whisper']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => $config->get('whisper.enable'),
      '#description' => $this->t('Enable or disable the whispering feature, which automatically bans IPs that trigger a threshold of 4xx responses.'),
    ];
    $form['whisper']['details'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="whisper[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['whisper']['details']['leak_speed'] = [
      '#type' => 'number',
      '#title' => $this->t('Time period'),
      '#description' => $this->t('Time period in seconds in which the number of bad requests (see threshold below) need to be recognized to get the IP banned.'),
      '#default_value' => $config->get('whisper.leak_speed'),
      '#min' => 1,
    ];
    $form['whisper']['details']['bucket_capacity'] = [
      '#type' => 'number',
      '#title' => $this->t('Threshold'),
      '#description' => $this->t('Number of bad requests in any given time period needed to get the IP banned.'),
      '#default_value' => $config->get('whisper.bucket_capacity'),
      '#min' => 1,
    ];
    $form['whisper']['details']['ban_duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Duration'),
      '#description' => $this->t('Time period in seconds for how long the IP should remain banned.'),
      '#default_value' => $config->get('whisper.ban_duration'),
      '#min' => 1,
    ];

    $form['signal_scenarios'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Signal scenarios'),
      '#options' => $this->signalScenarios(),
      '#default_value' => $config->get('signal_scenarios'),
      '#description' => $this->t('Choose the scenarios that will signal IPs to be blocked by CrowdSec.'),
    ];

    $form['scenarios'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Subscribe scenarios'),
      '#options' => $this->scenarios(),
      '#default_value' => $config->get('scenarios'),
      '#description' => $this->t('Select which attack scenarios you want your Drupal site to subscribe to. These scenarios will trigger action against suspicious IPs.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    if (empty($this->cleanupMap($form_state->getValue('scenarios')))) {
      $form_state->setErrorByName('scenarios', $this->t('At least one scenario has to be enabled.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('crowdsec.settings');
    $config
      ->set('log_level', $form_state->getValue('log_level'))
      ->set('env', $form_state->getValue('env'))
      ->set('api_timeout', $form_state->getValue('api_timeout'))
      ->set('cti_api_key', $form_state->getValue('cti_api_key'))
      ->set('whisper.enable', $form_state->getValue([
        'whisper',
        'enable',
      ]))
      ->set('whisper.leak_speed', $form_state->getValue([
        'whisper',
        'details',
        'leak_speed',
      ]))
      ->set('whisper.bucket_capacity', $form_state->getValue([
        'whisper',
        'details',
        'bucket_capacity',
      ]))
      ->set('whisper.ban_duration', $form_state->getValue([
        'whisper',
        'details',
        'ban_duration',
      ]))
      ->set('signal_scenarios', $this->cleanupMap($form_state->getValue('signal_scenarios')))
      ->set('scenarios', $this->cleanupMap($form_state->getValue('scenarios')))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Transforms a map into an array of selected elements.
   *
   * @param array $map
   *   The map.
   *
   * @return array
   *   The resulting list.
   */
  private function cleanupMap(array $map): array {
    $result = [];
    foreach ($map as $item) {
      if ($item) {
        $result[] = $item;
      }
    }
    return $result;
  }

}
