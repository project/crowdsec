<?php

declare(strict_types=1);

namespace Drupal\crowdsec;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Provides services around the Crowdsec CTI API.
 */
final class Cti implements CtiInterface {

  protected const API_URL = 'https://cti.api.crowdsec.net/v2/';

  /**
   * Constructs a Cti object.
   */
  public function __construct(
    protected readonly ClientFactory $clientFactory,
    protected readonly ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getSmoke(string $ip): array {
    $headers = [
      'accept' => 'application/json',
      'x-api-key' => $this->configFactory->get('crowdsec.settings')->get('cti_api_key'),
    ];
    $options['headers'] = $headers;
    try {
      $client = $this->clientFactory->fromOptions(['base_uri' => self::API_URL]);
      $response = $client->request('GET', 'smoke/' . $ip, $options);
      $statusCode = $response->getStatusCode();
      if ($statusCode === 200) {
        $content = $response->getBody()->getContents();
        return json_decode($content, TRUE);
      }
    }
    catch (GuzzleException) {
      // @todo Log this exception.
    }
    return [];
  }

}
