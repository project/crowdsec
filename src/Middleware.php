<?php

namespace Drupal\crowdsec;

use CrowdSec\Common\Constants;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\crowdsec\Event\CrowdSecEvents;
use Drupal\crowdsec\Event\IpBlocked;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides a HTTP middleware to implement CrowdSec based IP blocking.
 */
class Middleware implements HttpKernelInterface {

  /**
   * Flags if the middleware is active, can be disabled with ::disable().
   *
   * @var bool
   */
  protected static bool $active = TRUE;

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * The CrowdSec client service.
   *
   * @var \Drupal\crowdsec\Client
   */
  protected Client $client;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Constructs a CrowdSec Middleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\crowdsec\Client $client
   *   The CrowdSec client service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service.
   */
  public function __construct(HttpKernelInterface $http_kernel, Client $client, LoggerChannelInterface $logger, EventDispatcherInterface $eventDispatcher) {
    $this->httpKernel = $http_kernel;
    $this->client = $client;
    $this->logger = $logger;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Turns off this middleware for the current request.
   */
  public static function disable(): void {
    self::$active = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = HttpKernelInterface::MAIN_REQUEST, $catch = TRUE): Response {
    if (self::$active && $this->client->isConfigured()) {
      $ip = $request->getClientIp();
      if ($ip !== NULL && $this->client->verifyIp($ip) === Constants::REMEDIATION_BAN) {
        $this->logger->notice('Blocked @ip, details see <a href="@url">dashboard</a>', [
          'ip' => $ip,
          '@ip' => $ip,
          '@url' => Client::CROWDSEC_URL_CTI . $ip,
        ]);
        $this->eventDispatcher->dispatch(new IpBlocked($ip), CrowdSecEvents::IP_BLOCKED);
        return new Response(new FormattableMarkup('@ip has been blocked', ['@ip' => $ip]), 403);
      }
    }
    return $this->httpKernel->handle($request, $type, $catch);
  }

}
