<?php

namespace Drupal\crowdsec;

use CrowdSec\CapiClient\Storage\StorageInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;

/**
 * Implementation of the CrowdSec storage interface.
 */
class Storage implements StorageInterface {

  /**
   * The key-value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected KeyValueStoreInterface $keyValue;

  /**
   * Constructs the storage implementation.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key-value factory service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   */
  public function __construct(KeyValueFactoryInterface $keyValueFactory, ConfigFactoryInterface $configFactory) {
    $this->keyValue = $keyValueFactory->get('crowdsec');
    $env = $configFactory->get('crowdsec.settings')->get('env');
    if ($env !== $this->keyValue->get('env', 'undefined')) {
      $this->keyValue->delete('machine_id');
      $this->keyValue->delete('password');
      $this->keyValue->delete('token');
      $this->keyValue->set('env', $env);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveMachineId(): ?string {
    return $this->keyValue->get('machine_id');
  }

  /**
   * {@inheritdoc}
   */
  public function retrievePassword(): ?string {
    return $this->keyValue->get('password');
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveScenarios(): ?array {
    return $this->keyValue->get('scenarios');
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveToken(): ?string {
    return $this->keyValue->get('token');
  }

  /**
   * {@inheritdoc}
   */
  public function storeMachineId(string $machineId): bool {
    $this->keyValue->set('machine_id', $machineId);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function storePassword(string $password): bool {
    $this->keyValue->set('password', $password);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function storeScenarios(array $scenarios): bool {
    $this->keyValue->set('scenarios', $scenarios);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function storeToken(string $token): bool {
    $this->keyValue->set('token', $token);
    return TRUE;
  }

}
