<?php

namespace Drupal\crowdsec;

use CrowdSec\CapiClient\ClientException;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Link;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\ban\BanIpManagerInterface;
use Drupal\crowdsec\Event\CrowdSecEvents;
use Drupal\crowdsec\Event\IpBanned;
use Drupal\crowdsec\Event\IpSignalled;
use Drupal\crowdsec\Form\Settings;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Implements the CrowdSec buffer service.
 */
class Buffer {

  private const LAST_SIGNAL_PUSH_TIMESTAMP = 'crowdsec.signal.push.timestamp';
  private const SIGNALS = 'signals';
  private const WHISPER = 'whisper';
  private const LOCK = 'crowdsec.signal';

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The key-value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected KeyValueStoreInterface $keyValue;

  /**
   * The expirable key-value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected KeyValueStoreExpirableInterface $expirableKeyValue;

  /**
   * The lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The CrowdSec client service.
   *
   * @var \Drupal\crowdsec\Client
   */
  protected Client $client;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The CrowdSec configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The ban IP manager.
   *
   * @var \Drupal\ban\BanIpManagerInterface
   */
  protected BanIpManagerInterface $banIpManager;

  /**
   * The queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected QueueInterface $queue;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Constructs the storage implementation.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key-value factory service.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $keyValueExpirableFactory
   *   The expirable key-value factory service.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The locking backend service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\crowdsec\Client $client
   *   The CrowdSec client service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\ban\BanIpManagerInterface $banIpManager
   *   The ban IP manager.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service.
   */
  public function __construct(StateInterface $state, KeyValueFactoryInterface $keyValueFactory, KeyValueExpirableFactoryInterface $keyValueExpirableFactory, LockBackendInterface $lock, TimeInterface $time, Client $client, LoggerChannelInterface $logger, ConfigFactoryInterface $configFactory, BanIpManagerInterface $banIpManager, QueueFactory $queueFactory, EventDispatcherInterface $eventDispatcher) {
    $this->state = $state;
    $this->keyValue = $keyValueFactory->get('crowdsec');
    $this->expirableKeyValue = $keyValueExpirableFactory->get('crowdsec');
    $this->lock = $lock;
    $this->time = $time;
    $this->client = $client;
    $this->logger = $logger;
    $this->config = $configFactory->get('crowdsec.settings');
    $this->banIpManager = $banIpManager;
    $this->queue = $queueFactory->get('crowdsec', TRUE);
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Acquires a lock for CrowdSec to safely operate some non-concurrent actions.
   *
   * @return bool
   *   TRUE, if successful, FALSE otherwise.
   */
  private function lock(): bool {
    $attempts = 0;
    while (!$this->lock->acquire($this::LOCK)) {
      $attempts++;
      if ($attempts > 3) {
        return FALSE;
      }
      $this->lock->wait($this::LOCK, 1);
    }
    return TRUE;
  }

  /**
   * Adds a new signal to the buffer for being sent later.
   *
   * @param string $scenario
   *   The scenario name.
   * @param string $ip
   *   The IP address to be blocked.
   * @param int $duration
   *   The duration in seconds for how long the IP should be blocked.
   */
  public function addSignal(string $scenario, string $ip, int $duration): void {
    if (!in_array($scenario, $this->config->get('signal_scenarios'), TRUE)) {
      $this->logger->info('Ignoring signal for @ip because scenario @scenario is disabled.', [
        '@scenario' => $scenario,
        '@ip' => $ip,
      ]);
      return;
    }
    if (!$this->lock()) {
      // We can not acquire a lock and therefore can't buffer the new signal.
      $this->logger->critical('Can not acquire lock. Wanted to add signal to @scenario for @ip.', [
        '@scenario' => $scenario,
        '@ip' => $ip,
      ]);
      return;
    }
    $signals = $this->keyValue->get($this::SIGNALS, []);
    $signals[] = [
      'scenario' => $scenario,
      'ip' => $ip,
      'duration' => $duration,
      'timestamp' => $this->time->getRequestTime(),
    ];
    $this->keyValue->set($this::SIGNALS, $signals);
    $this->lock->release($this::LOCK);
    $this->logger->warning('Buffered signal to @scenario for @ip.', [
      '@scenario' => $scenario,
      '@ip' => $ip,
      'link' => Link::fromTextAndUrl('Link', Url::fromUri(Client::CROWDSEC_URL_CTI . $ip))->toString(),
    ]);
    $this->eventDispatcher->dispatch(new IpSignalled($ip, $scenario), CrowdSecEvents::IP_SIGNALLED);
  }

  /**
   * Adds a new whisper signal to the buffer.
   *
   * @param string $ip
   *   The IP address.
   */
  public function addWhisperSignal(string $ip): void {
    if (!$this->config->get('whisper.enable')) {
      $this->logger->info('Ignoring whisper signal for @ip because it is disabled.', [
        '@ip' => $ip,
      ]);
      return;
    }
    $key = implode('-', [$this::WHISPER, $ip]);
    $duration = $this->config->get('whisper.duration') ?? 3600;
    $leakSpeed = $this->config->get('whisper.leak_speed') ?? 10;
    $bucketCapacity = $this->config->get('whisper.bucket_capacity') ?? 10;
    $requestTime = $this->time->getRequestTime();

    $bucketFill = $this->expirableKeyValue->get($key . '_whisper_count', 0);
    $lastWhisperTime = $this->expirableKeyValue->get($key . '_whisper_latest_time', $requestTime);
    $bucketFill++;
    $bucketFill -= floor(($requestTime - $lastWhisperTime) / $leakSpeed);

    if ($bucketFill > $bucketCapacity) {
      // Threshold reached, take actions.
      // - ban the ip.
      $this->banIpManager->banIp($ip);
      $this->logger->info('Banned ip @ip', [
        '@ip' => $ip,
        'link' => Link::fromTextAndUrl('Link', Url::fromUri(Client::CROWDSEC_URL_CTI . $ip))->toString(),
      ]);
      $this->eventDispatcher->dispatch(new IpBanned($ip), CrowdSecEvents::IP_BANNED);
      // - add task to unban the ip.
      $this->queue->createItem([
        'type' => 'unban',
        'ip' => $ip,
        'due' => $requestTime + $duration,
      ]);
      // - add signal.
      $this->addSignal(Settings::SCENARIO_WHISPER, $ip, $duration);
    }

    if ($bucketFill <= 0) {
      // Delete history.
      $this->expirableKeyValue->delete($key . '_whisper_count');
      $this->expirableKeyValue->delete($key . '_whisper_latest_time');
    }
    else {
      // Threshold not reached yet, update history.
      $expire = (int) (2 * $leakSpeed * $bucketFill);
      $this->expirableKeyValue->setWithExpire($key . '_whisper_count', $bucketFill, $expire);
      $this->expirableKeyValue->setWithExpire($key . '_whisper_latest_time', $requestTime, $expire);
    }
  }

  /**
   * Push buffered signals to CrowdSec.
   */
  public function push(): void {
    $lastPush = $this->state->get($this::LAST_SIGNAL_PUSH_TIMESTAMP, 0);
    $now = $this->time->getCurrentTime();
    if ($lastPush + 10 > $now) {
      // It's too early, wait for the next round.
      return;
    }
    $watcher = $this->client->watcher();
    $pushSignals = [];
    if (!$this->lock()) {
      // We can not acquire a lock and therefore can't safely push.
      $this->logger->critical('Can not acquire lock. Wanted to push signals upstream.');
      return;
    }
    $signals = $this->keyValue->get($this::SIGNALS, []);
    $i = 0;
    while ($signal = array_shift($signals)) {
      $i++;
      if ($i > 250) {
        break;
      }
      $datetime = new \DateTime();
      $datetime->setTimestamp($signal['timestamp']);
      try {
        $pushSignals[] = $watcher->buildSimpleSignalForIp(
          $signal['ip'],
          $signal['scenario'],
          $datetime,
          '',
          $signal['duration']
        );
      }
      catch (ClientException $e) {
        $this->logger->error('Can not create signal on scenario @scenario for IP @ip: $message', [
          '@scenario' => $signal['scenario'],
          '@ip' => $signal['ip'],
          '@message' => $e->getMessage(),
        ]);
      }
    }
    if (!empty($pushSignals)) {
      $watcher->pushSignals($pushSignals);
      $this->logger->info('Pushed @count signals upstream.', [
        '@count' => $i,
      ]);
    }
    $this->keyValue->set($this::SIGNALS, $signals);
    $this->lock->release($this::LOCK);
    $this->state->set($this::LAST_SIGNAL_PUSH_TIMESTAMP, $now);
  }

}
