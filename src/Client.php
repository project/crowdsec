<?php

namespace Drupal\crowdsec;

use CrowdSec\CapiClient\Storage\StorageInterface;
use CrowdSec\CapiClient\Watcher;
use CrowdSec\Common\Constants;
use CrowdSec\RemediationEngine\CacheStorage\AbstractCache;
use CrowdSec\RemediationEngine\CacheStorage\PhpFiles;
use CrowdSec\RemediationEngine\CacheStorage\Redis;
use CrowdSec\RemediationEngine\CapiRemediation;
use CrowdSec\RemediationEngine\Constants as RemediationConstants;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Site\Settings;
use Psr\Cache\CacheException;
use Psr\Cache\InvalidArgumentException;

/**
 * Provides the CrowdSec watcher interface.
 */
class Client {

  public const CROWDSEC_URL_CTI = 'https://app.crowdsec.net/cti/';

  /**
   * The CrowdSec configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The storage service.
   *
   * @var \CrowdSec\CapiClient\Storage\StorageInterface
   */
  protected StorageInterface $storage;

  /**
   * The cache for the remediation engine.
   *
   * @var \CrowdSec\RemediationEngine\CacheStorage\AbstractCache|null
   */
  protected ?AbstractCache $cache;

  /**
   * The CrowdSec watcher.
   *
   * @var \CrowdSec\CapiClient\Watcher|null
   */
  protected ?Watcher $watcher;

  /**
   * The CrowdSec remediation engine.
   *
   * @var \CrowdSec\RemediationEngine\CapiRemediation|null
   */
  protected ?CapiRemediation $remediation;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Constructs the CrowdSec client implementation.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \CrowdSec\CapiClient\Storage\StorageInterface $storage
   *   The storage service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, StorageInterface $storage, LoggerChannelInterface $logger, FileSystemInterface $fileSystem) {
    $this->config = $configFactory->get('crowdsec.settings');
    $this->storage = $storage;
    $this->logger = $logger;
    $this->fileSystem = $fileSystem;
  }

  /**
   * Determines if the module has been configured.
   *
   * @return bool
   *   TRUE, if it's configured, FALSE otherwise.
   */
  public function isConfigured(): bool {
    return in_array($this->config->get('env') ?? '', ['prod', 'dev'], TRUE);
  }

  /**
   * Initializes the CrowdSec watcher.
   *
   * @return \CrowdSec\CapiClient\Watcher
   *   The CrowdSec watcher.
   */
  public function watcher(): Watcher {
    if (!isset($this->watcher)) {
      $configs = [
        'env' => $this->config->get('env'),
        'api_timeout' => (int) $this->config->get('api_timeout'),
        'scenarios' => $this->config->get('scenarios'),
        'user_agent_suffix' => 'drupal',
        'user_agent_version' => 'v1.0.0',
      ];
      $this->watcher = new Watcher($configs, $this->storage, NULL, $this->logger);
    }
    return $this->watcher;
  }

  /**
   * Initializes the CrowdSec remediation client.
   *
   * @return \CrowdSec\RemediationEngine\CapiRemediation
   *   The remediation client.
   */
  public function remediation(): CapiRemediation {
    if (!isset($this->remediation)) {
      $this->remediation = new CapiRemediation([], $this->watcher(), $this->cache(), $this->logger);
    }
    return $this->remediation;
  }

  /**
   * Refresh data from CrowdSec.
   */
  public function refresh(): void {
    try {
      $this->remediation()->refreshDecisions();
    }
    catch (InvalidArgumentException | CacheException $e) {
      $this->logger->critical($e->getMessage());
    }
  }

  /**
   * Verifies the IP against the CrowdSec remediation.
   *
   * @param string $ip
   *   The IP address to verify.
   *
   * @return string
   *   The remediation string.
   */
  public function verifyIp(string $ip): string {
    $remediation = $this->remediation();
    try {
      return $remediation->getIpRemediation($ip)[RemediationConstants::REMEDIATION_KEY];
    }
    catch (InvalidArgumentException | CacheException $e) {
      $this->logger->critical('Unable to receive remediation for @ip: @message', [
        '@ip' => $ip,
        '@message' => $e->getMessage(),
      ]);
    }
    return Constants::REMEDIATION_BYPASS;
  }

  /**
   * Initialize and return the cache backend.
   *
   * @return \CrowdSec\RemediationEngine\CacheStorage\AbstractCache
   *   The initialized cache backend.
   */
  private function cache(): AbstractCache {
    if (!isset($this->cache)) {
      if ($redis = Settings::get('redis.connection')) {
        // Construct the Redis DSN with optional username and password.
        $host = $redis['host'] ?? 'localhost';
        $port = $redis['port'] ?? '6379';
        $password = $redis['password'] ?? NULL;

        $dsn = 'redis://';
        if ($password) {
          $username = $redis['username'] ?? NULL;
          if ($username) {
            $dsn .= $username . ':';
          }
          $dsn .= $password . '@';
        }
        $dsn .= $host . ':' . $port;

        $this->cache = new Redis(['redis_dsn' => $dsn], $this->logger);
      }
      else {
        $this->cache = new PhpFiles(['fs_cache_path' => $this->fileSystem->getTempDirectory() . '/crowdsec'], $this->logger);
      }
    }
    return $this->cache;
  }

}
