<?php

namespace Drupal\crowdsec;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Decorator that applies currently active logging settings.
 */
class ConfigurableLoggerChannel extends LoggerChannel {

  /**
   * The logger channel that is being decorated by this service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerChannel;

  /**
   * The maximum allowed RFC log level.
   *
   * @var int
   */
  protected int $maximumLogLevel;

  /**
   * The ConfigurableLoggerChannel constructor.
   *
   * @param string $channel_name
   *   The name of the logger channel that is being decorated.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   The logger channel that is being decorated by this service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(string $channel_name, LoggerChannelInterface $loggerChannel, ConfigFactoryInterface $configFactory) {
    parent::__construct($channel_name);
    $this->loggerChannel = $loggerChannel;
    $this->maximumLogLevel = (int) $configFactory->get('crowdsec.settings')->get('log_level');
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    if (is_string($level)) {
      // Convert to integer equivalent for consistency with RFC 5424.
      $level = $this->levelTranslation[$level];
    }
    if ($level <= $this->maximumLogLevel) {
      if (isset($context['type'])) {
        $message .= PHP_EOL . '<code>@detail</code>';
        try {
          $context = [
            '@detail' => json_encode($context, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR),
          ];
        }
        catch (\JsonException) {
          // We can not log this exception, that would lead to log loop.
          $context = [
            '@detail' => serialize($context),
          ];
        }
      }
      $this->loggerChannel->log($level, $message, $context);
    }
  }

}
