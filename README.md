Integrates your Drupal site with [CrowdSec](https://www.crowdsec.net)
to keep suspicious users and cybercriminals away. No account with CrowdSec
required, and no other agent software to be installed. This module brings 
everything required with it. Download with composer, enable the module and
rest assured, that a lot of malicious traffic and many attack vectors will
just be gone!

It's that simple.

## How does it work?

CrowdSec collects malicious IP addresses globally from everybody who is
willing to collaborate. There are many
[supported "scenarios"](https://hub.crowdsec.net/browse"), under which 
bad actors and their IP addresses can be reported. Once CrowdSec is 
confident because the signals about an IP address are strong enough,
that IP gets added to their public block list for a period of time.

This module integrates with the CrowdSec API using their PHP SDK to verify
the IP address of each request getting to your Drupal site against their 
block list and should an IP be listed for being banned, those requests will
be rejected and responded with a 403 HTTP response status code.

No worries, the block list is cached locally, and no performance 
implications are expected.

### Important note about caching

This module allows using Redis, if your Drupal site configuration contains
<code>$settings['redis.connection']</code> and will therefore use the 
available Redis host, that your site already uses for the CrowdSec cache
as well. You need to enable that in the settings of this module.

As a fallback, it uses the file system and stores the cache in
<code>temporary://crowdsec</code>.

## How does your Drupal site contribute to the CrowdSec signal collection?

To receive the free block list through the CrowdSec API, your Drupal site 
needs to participate in the community effort to identify malicious IP 
addresses. When you enable this module, this will happen by default and
automatically in the background. Note: you remain controlling what's 
being pushed upstream, each signal type can be disabled in the module's
settings.

### A note about privacy and compliance

This question has been asked before: is it a potential compliance issue
and a breach of the user's privacy, when this module signals malicious
behavior upstream to CrowdSec?

We certainly don't think so, especially because the sole reason for the
potential signal is to protect the website from some strange behavior.
This is technically not only necessary but therefor an exempt from the
data protection regulations. Of course, we're no lawyers, and you shouldn't
take this as legal advise in any context. But we can provide you with a list
of links that seem to confirm our understanding:

- [Issue about this question](https://www.drupal.org/project/crowdsec/issues/3471010)
- [Discussion on Discourse](https://discourse.crowdsec.net/t/is-crowdsec-acting-against-european-privacy-regulations/1363/4)
- [Article 49 of EU GDPR](https://www.privacy-regulation.eu/en/recital-49-GDPR.htm)

### Available signal types

This module provides three different signal types:


**Flood control**: Drupal core comes with an integrated flood control 
system which e.g. bans IP addresses or user accounts for a period of time,
if a number of unsuccessful login attempts have been recognized.

**Ban module**: this optional module allows to ban IP addresses manually.

**Whispering**: this is provided by this module and automatically bans an
IP address for a period of time, if it causes a number of 4xx responses in
a specified period of time.

By default, these signal types are enabled and will push the IP addresses
upstream to CrowdSec while the blocking of those happens locally, independent
of CrowdSec including them into their block list.

### Extending this module and roadmap

This module is built such that additional signals can be added easily. This
can either be done in this module directly by opening a feature request in 
the issue queue and collaborating here. Or, new signals can also be provided
in separate modules here on d.o or even in custom code.

In addition to that, we're planning to enhance this module's capabilities 
together with the team at CrowdSec. For example, there will be the option 
to use captchas for certain actions instead of blocking an IP entirely.

But first, let's get the current feature set tested and stabilized. 
Shouldn't take long, until we get to a stable release. The more feedback we
get, the better.
